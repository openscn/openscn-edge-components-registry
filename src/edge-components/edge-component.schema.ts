/**
 *
 * The Edge component schema
 * See the edge component interface for these fields description and values
 *
 */

import * as mongoose from 'mongoose';
import { DependentComponentsSchema } from './../dependent-components/dependent-components.schema';

export const EdgeComponentSchema: mongoose.Schema = new mongoose.Schema(
  {
    uuid: {
      type: String,
      unique: true,
    },
    uuidHistory: Array,
    specifications: Object,
    dataFlow: String,
    dependentComponents: [DependentComponentsSchema],
    authStrategies: Object,
    trustStatusHistory: Array,
    healthHistory: Array,
    isActive: Boolean,
    lastActivity: Object,
    createdAt: Date,
    updatedAt: Date,
  },
  {
    collection: 'edge-components',
  },
);
