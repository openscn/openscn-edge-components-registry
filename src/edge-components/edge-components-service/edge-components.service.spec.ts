import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { EdgeComponentsService } from './edge-components.service';
import { EdgeComponentsController } from '../edge-components-controller/edge-components.controller';
import {
  StorageTestingModule,
  closeDatabase,
} from './../../lib/storage/storage-testing.module';
import { EdgeComponentSchema } from '../edge-component.schema';
import { DependentComponentsModule } from './../../dependent-components/dependent-components.module';

describe('EdgeComponentsService', () => {
  let service: EdgeComponentsService;
  let module: TestingModule;

  beforeAll(() => {
    process.env.OPENSCN_EC_MNG_TOKEN = 'mockToken';
    process.env.OPENSCN_EC_EC_TOKEN = 'mockToken';
  });

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        StorageTestingModule,
        DependentComponentsModule,
        MongooseModule.forFeature([
          { name: 'EdgeComponent', schema: EdgeComponentSchema },
        ]),
      ],
      controllers: [EdgeComponentsController],
      providers: [EdgeComponentsService],
    }).compile();

    service = module.get<EdgeComponentsService>(EdgeComponentsService);
  });

  afterAll(async () => {
    await closeDatabase();
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('The find most recent function', () => {
    it('should sort an array based on updatedAt', () => {
      const mockArray = [
        { updatedAt: new Date('2019-03-02'), status: 'mock1' },
        { updatedAt: new Date('2019-02-02'), status: 'mock2' },
        { updatedAt: new Date('2019-01-03'), status: 'mock3' },
      ];

      const result = service.findMostRecent(mockArray);
      expect(result.status).toEqual('mock1');

      mockArray.push({ updatedAt: new Date('2020-01-01'), status: 'mock4' });
      const result2 = service.findMostRecent(mockArray);
      expect(result2.status).toEqual('mock4');
    });
  });
});
