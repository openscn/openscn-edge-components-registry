import {
  Injectable,
  NotFoundException,
  NotImplementedException,
  InternalServerErrorException,
  ConflictException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { EdgeComponent } from './../edge-component.interface';
import {
  CreateEdgeComponentDTO,
  UpdateEdgeComponentDTO,
  AssignComponentDTO,
  UnassignComponentDTO,
  DeleteComponentDTO,
} from './../dto/edge-component.dto';
import { DependentComponentsService } from './../../dependent-components/dependent-components-service/dependent-components.service';
import { DependentComponent } from 'src/dependent-components/dependent-components.interface';
import { StatusUnit } from './../edge-components';

@Injectable()
export class EdgeComponentsService {
  constructor(
    @InjectModel('EdgeComponent')
    private edgeComponentModel: Model<EdgeComponent>,
    private dependentComponentsService: DependentComponentsService,
  ) {}

  findMostRecent(array: StatusUnit[]) {
    const sorted = array.sort(
      (a, b) => b.updatedAt.getTime() - a.updatedAt.getTime(),
    );
    return sorted[0];
  }

  getReadable(component: EdgeComponent): EdgeComponent {
    const currentHealth = this.findMostRecent(component.healthHistory);
    const currentTrustStatus = this.findMostRecent(
      component.trustStatusHistory,
    );
    const readable: EdgeComponent = component;
    component.health = currentHealth.status;
    component.trustStatus = currentTrustStatus.status;
    return readable;
  }

  /**
   *
   * Initializes a new edge component
   *
   */
  initializeNewComponent(dto: CreateEdgeComponentDTO) {
    const component = new this.edgeComponentModel({
      dataFlow: dto.dataFlow,
      isArray: dto.isActive,
      specifications: {
        platformType: dto.platformType,
        supportedProtocol: 'HTTP',
      },
      trustStatusHistory: [
        {
          status: 'unknown',
          updatedAt: new Date(),
        },
      ],
      healthHistory: [
        {
          status: 'unknown',
          updatedAt: new Date(),
        },
      ],
      authStrategies: {
        incoming: [],
        outgoing: [],
      },
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    return component;
  }

  resolveDateFlow(component: EdgeComponent) {
    const authorizationStrategy = {
      // right now this is the only supported one
      type: 'uuid-equality',
      default: true,
    };

    // From the server perspective
    if (component.dataFlow === 'both') {
      component.authStrategies.incoming.push(authorizationStrategy);
      component.authStrategies.outgoing.push(authorizationStrategy);
    } else if (component.dataFlow === 'out') {
      component.authStrategies.outgoing.push(authorizationStrategy);
    } else if (component.dataFlow === 'in') {
      component.authStrategies.incoming.push(authorizationStrategy);
    }

    return component;
  }

  /**
   *
   * Searches for a single edge component
   *
   * @param {string} uuid The uuid of the component
   *
   */
  async findOne(uuid: string): Promise<EdgeComponent> {
    let component: EdgeComponent;

    try {
      component = await this.edgeComponentModel.findOne({
        uuid,
      });
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error while fetching edge component ${uuid}`,
      );
    }

    return component;
  }

  /**
   *
   * Searches for multiple edge components
   *
   * @param {Array<string>} uuids The list of uuids to find
   *
   */
  async findMany(uuids: string[]): Promise<EdgeComponent[]> {
    let components;
    try {
      components = await this.edgeComponentModel.find({
        uuid: { $in: uuids },
      });
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        'Error while fething edge components',
      );
    }

    if (components) {
      return components;
    } else {
      throw new NotFoundException('Components could not be found');
    }
  }

  async create(dto: CreateEdgeComponentDTO) {
    let component = this.initializeNewComponent(dto);

    try {
      component.uuid = await this.generateUUID();
    } catch (err) {
      throw new InternalServerErrorException(
        `INTERNALERROR`,
        `Could not generate uuid`,
      );
    }

    component = this.resolveDateFlow(component);

    try {
      await component.save();
      return component;
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error while saving new component`,
      );
    }
  }

  async updateOne(dto: UpdateEdgeComponentDTO) {
    let component: EdgeComponent;
    try {
      component = await this.findOne(dto.uuid);
    } catch (err) {
      throw err;
    }

    if (!component) {
      throw new NotFoundException(
        `NOTFOUNDERROR`,
        `Component ${dto.uuid} could not be found`,
      );
    }

    delete dto.uuid;

    for (const attribute of Object.keys(dto)) {
      component[attribute] = dto[attribute];
    }

    let updated: EdgeComponent;
    try {
      updated = await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error while updating component ${dto.uuid}`,
      );
    }

    return updated;
  }

  async delete(dto: DeleteComponentDTO) {
    let component: EdgeComponent;

    try {
      component = await this.findOne(dto.uuid);
    } catch (err) {
      throw err;
    }

    if (!component) {
      throw new NotFoundException(
        'NOTFOUNDERRROR',
        `Component ${dto.uuid} could not be found`,
      );
    }

    const deletedDependents = [];
    const deleteErrors = [];
    if (dto.deleteDependents) {
      component.dependentComponents.forEach(
        async (depended: DependentComponent) => {
          try {
            await this.dependentComponentsService.deleteOne(depended.uuid);
            deletedDependents.push({
              uuid: depended.uuid,
            });
          } catch (err) {
            deleteErrors.push({
              uuid: depended.uuid,
              error: err,
            });
          }
        },
      );
    }

    if (deleteErrors.length === 0) {
      try {
        await this.edgeComponentModel.deleteOne({
          uuid: dto.uuid,
        });
        return component;
      } catch (err) {
        throw new InternalServerErrorException(
          'DBERROR',
          `Could not delete edge component ${dto.uuid}`,
        );
      }
    } else {
      throw new InternalServerErrorException(
        'DBERROR',
        `Could not delete ${deleteErrors.length} dependent components of edge component ${dto.uuid}`,
      );
    }
  }

  async generateUUID(): Promise<string> {
    // TODO: ensure that it is unique
    const uuid = uuidv4();

    try {
      const existingComponent = await this.findOne(uuid);
      if (existingComponent) {
        throw new ConflictException(
          `INTERNALERROR`,
          `UUID ${uuid} already exist`,
        );
      }
    } catch (err) {
      throw err;
    }

    return uuid;
  }

  async updateUUID(uuid: string) {
    let component: EdgeComponent;
    try {
      component = await this.findOne(uuid);
      if (!component) {
        throw new NotFoundException(
          'NOTFOUNDERRPR',
          `The edge component ${uuid} was not foung`,
        );
      }
    } catch (err) {
      throw err;
    }

    const currentUUID = component.uuid;
    component.uuidHistory.push(currentUUID);
    component.uuid = await this.generateUUID();

    try {
      await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Edge component ${uuid} could not be saved`,
      );
    }
    return component;
  }

  async updateSpecifications(uuid: string, specifications) {
    throw new NotImplementedException();
  }

  /**
   *
   * Assigns a dependent component to the edge component
   *
   */
  async assignComponent(dto: AssignComponentDTO) {
    let component: EdgeComponent;
    let dependent: DependentComponent;

    try {
      component = await this.findOne(dto.uuid);
      dependent = await this.dependentComponentsService.findOne(dto.dependent);

      if (component && dependent) {
        const foundDependent = component.dependentComponents.find(
          dep => dep.uuid === dto.dependent,
        );

        if (foundDependent) {
          throw new ConflictException(
            'CONFLICT',
            `Dependent component ${dto.dependent} exist on component ${component.uuid}`,
          );
        }
      }

      if (!component) {
        throw new NotFoundException(
          `NOTFOassignUNDERROR`,
          `Edge component ${dto.uuid} was not founf`,
        );
      } else if (!dependent) {
        throw new NotFoundException(
          `NOTFOUNDERROR`,
          `Dependent component ${dto.dependent} was not found`,
        );
      }
    } catch (err) {
      throw err;
    }

    component.dependentComponents.push(dependent);

    let savedComponent: EdgeComponent;
    try {
      savedComponent = await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        `DBERROR`,
        `Dependent component ${dto.dependent} could not be assigned to ${dto.uuid}`,
      );
    }

    return savedComponent;
  }

  /**
   *
   * Unassigns a dependent component from an edge component
   *
   * @param dto The unassignment dto
   *
   */
  async unassignComponent(dto: UnassignComponentDTO) {
    let component: EdgeComponent;

    try {
      component = await this.findOne(dto.uuid);
      if (!component) {
        throw new NotFoundException(
          `NOTFOUNDERROR`,
          `Edge component ${dto.uuid} was not found`,
        );
      }

      const dependentFound = component.dependentComponents.find(
        dependent => dependent.uuid === dto.dependent,
      );

      if (!dependentFound) {
        throw new ConflictException(
          `CONFLICT`,
          `The dependent component is not child of the ${dto.uuid}`,
        );
      }
    } catch (err) {
      throw err;
    }

    component.dependentComponents = component.dependentComponents.filter(
      dependent => dependent.uuid !== dto.dependent,
    );

    let updated: EdgeComponent;
    try {
      updated = await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        `DBERROR`,
        `The edge component ${component.uuid} could not be saved durring dependent ${dto.dependent} attachment`,
      );
    }

    return updated;
  }

  async updateTrustStatus(uuid: string, trustStatus: string) {
    const component = await this.findOne(uuid);

    const newTrustStatus = {
      status: trustStatus,
      updatedAt: new Date(),
    };

    component.trustStatusHistory.push(newTrustStatus);
    await component.save();
    return component;
  }

  async updateHealthStatus(uuid: string, healthStatus: string) {
    const component = await this.findOne(uuid);

    const newHealthStatus = {
      status: healthStatus,
      updatedAt: new Date(),
    };

    component.healthHistory.push(newHealthStatus);
    await component.save();
    return component;
  }
}
