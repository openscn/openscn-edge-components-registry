/**
 *
 * The DTOs for the edge components
 *
 */
import {
  IsString,
  IsBoolean,
  IsIn,
  IsUUID,
  IsArray,
  IsOptional,
} from 'class-validator';

/**
 *
 * @class CreateEdgeComponentDTO
 * The fields that create a new edge component
 *
 */

export class CreateEdgeComponentDTO {
  /**
   * @property {boolean} isActive The flag the indicates whether the component
   * is active. Default is true
   */
  @IsBoolean()
  isActive: boolean;

  /**
   * @property {string} componentType The platform type of the component.
   * Allowed values ("mc", "pc")
   */
  @IsString()
  @IsIn(['mc', 'pc'])
  platformType: string;

  /**
   * @property {string} dataFlow The direction of the data for the system perspective
   * Allowed values ("in", "out", "both")
   */
  @IsString()
  @IsIn(['in', 'out', 'both'])
  dataFlow: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateEdgeComponentDTO {
  @IsUUID(4)
  uuid: string;

  @IsOptional()
  @IsBoolean()
  isActive: boolean;

  @IsOptional()
  @IsString()
  @IsIn(['in', 'out', 'both'])
  dataFlow: string;
}

// tslint:disable-next-line: max-classes-per-file
export class GetOneEdgeComponentDTO {
  @IsUUID(4)
  uuid: string;
}

// tslint:disable-next-line: max-classes-per-file
export class GetManyEdgeComponentDTO {
  @IsArray()
  uuids: string[];
}

// tslint:disable-next-line: max-classes-per-file
export class ResetUUIDDTO {
  @IsUUID(4)
  uuid: string;
}

// tslint:disable-next-line: max-classes-per-file
export class AssignComponentDTO {
  @IsUUID(4)
  uuid: string;

  @IsUUID(4)
  dependent: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UnassignComponentDTO {
  @IsUUID(4)
  uuid: string;

  @IsUUID(4)
  dependent: string;
}

// tslint:disable-next-line: max-classes-per-file
export class DeleteComponentDTO {
  @IsUUID(4)
  uuid: string;

  @IsOptional()
  @IsBoolean()
  deleteDependents: boolean;
}
