import { Test, TestingModule } from '@nestjs/testing';
import { EdgeComponentsController } from './edge-components.controller';
import { EdgeComponentsService } from '../edge-components-service/edge-components.service';
import {
  StorageTestingModule,
  closeDatabase,
} from '../../lib/storage/storage-testing.module';
import { MongooseModule } from '@nestjs/mongoose';
import { EdgeComponentSchema } from '../edge-component.schema';
import { DependentComponentsModule } from './../../dependent-components/dependent-components.module';
// import { AuthorizationModule } from './../../lib/authorization/authorization.module';

describe('EdgeComponents Controller', () => {
  let controller: EdgeComponentsController;
  let module: TestingModule;

  beforeAll(() => {
    process.env.OPENSCN_EC_MNG_TOKEN = 'mockToken';
    process.env.OPENSCN_EC_EC_TOKEN = 'mockToken';
  });

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        // AuthorizationModule,
        StorageTestingModule,
        DependentComponentsModule,
        MongooseModule.forFeature([
          { name: 'EdgeComponent', schema: EdgeComponentSchema },
        ]),
      ],
      controllers: [EdgeComponentsController],
      providers: [EdgeComponentsService],
    }).compile();

    controller = module.get<EdgeComponentsController>(EdgeComponentsController);
  });

  afterAll(async () => {
    await closeDatabase();
    await module.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
