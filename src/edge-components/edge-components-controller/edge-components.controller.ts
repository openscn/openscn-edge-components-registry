import {
  Controller,
  Post,
  Body,
  NotImplementedException,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { EdgeComponentsService } from '../edge-components-service/edge-components.service';
import {
  CreateEdgeComponentDTO,
  GetOneEdgeComponentDTO,
  GetManyEdgeComponentDTO,
  ResetUUIDDTO,
  UpdateEdgeComponentDTO,
  AssignComponentDTO,
  UnassignComponentDTO,
  DeleteComponentDTO,
} from '../dto/edge-component.dto';
import { ManagementApiTokenGuard } from './../../lib/authorization';

@Controller('edge-components')
@UseGuards(ManagementApiTokenGuard)
export class EdgeComponentsController {
  constructor(private readonly edgeComponentsService: EdgeComponentsService) {}

  /**
   *
   * @api {post} /edge-components/get-one Get one
   * @apiName Show
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Show a single edge component based on it's uuid
   * @apiParam {string} uuid The uuid of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('get-one')
  async findOne(@Body() dto: GetOneEdgeComponentDTO) {
    try {
      const component = await this.edgeComponentsService.findOne(dto.uuid);

      if (!component) {
        throw new NotFoundException(
          `NOTFOUNDERROR`,
          `Component ${dto.uuid} could not be found`,
        );
      }

      return component;
    } catch (err) {
      throw err;
    }
  }

  /**
   *
   * @api {post} /edge-components/get-many Get Many
   * @apiName List
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Show multiple components based on their uuids
   * @apiParam {string[]} uuids The uuids of the edge components
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('get-many')
  async findMany(@Body() dto: GetManyEdgeComponentDTO) {
    const components = await this.edgeComponentsService.findMany(dto.uuids);
    return components;
  }

  /**
   *
   * @api {post} /edge-components/create Create
   * @apiName Create
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Create a single edge component
   * @apiParam {boolean} [isActive=true] The status of the component
   * @apiParam {string="mc", "pc"} [platformType] The platform of the component
   * @apiParam {string="in", "out", "both"} dataflow The direction of the data
   *
   * @apiUse apiTokenHeader
   * @apiUse internalError
   *
   */
  @Post('create')
  async create(@Body() dto: CreateEdgeComponentDTO) {
    const component = this.edgeComponentsService.create(dto);
    return component;
  }

  /**
   *
   * @api {post} /edge-components/reset-uuid Reset uuid
   * @apiName ResetUUID
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Creates a new uuid and invalidates the old one
   * @apiParam {string} uuid The uuid of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('reset-uuid')
  async resetUUID(@Body() dto: ResetUUIDDTO) {
    const updatedComponent = await this.edgeComponentsService.updateUUID(
      dto.uuid,
    );
    return updatedComponent;
  }

  /**
   *
   * @api {post} /edge-components/update Update one
   * @apiName Update
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Update a single edge component
   * @apiParam {string} uuid The uuid of the edge component
   * @apiParam {boolean} [isActive] The status of the component
   * @apiParam {string="in", "out", "both"} [dataflow] The direction of the data
   *
   * @apiUse apiTokenHeader
   * @apiUse internalError
   *
   */
  @Post('update')
  async updateOne(@Body() dto: UpdateEdgeComponentDTO) {
    const component = await this.edgeComponentsService.updateOne(dto);
    return component;
  }

  /**
   * @apiIgnore Not implemented
   * @api {post} /edge-components/update Delete one
   * @apiName Delete
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Delete a single edge component
   * @apiParam {string} uuid The uuid of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('delete')
  async removeOne(@Body() dto: DeleteComponentDTO) {
    return await this.edgeComponentsService.delete(dto);
  }

  /**
   * @apiIgnore Not implemented
   * @api {post} /edge-components/delete-many Delete many
   * @apiName DeleteMany
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Delete multiple edge componentss
   * @apiParam {string} uuid The uuid of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('delete-many')
  async removeMany() {
    throw new NotImplementedException();
  }

  /**
   *
   * @api {post} /edge-components/attach-dependent Attach dependent
   * @apiName AttachDependent
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Attaches a dependent component to an edge component
   * @apiParam {string} uuid The uuid of the edge component
   * @apiParam {string} dependent The uuid of the dependent component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('attach-dependent')
  async assignDependentComponent(@Body() dto: AssignComponentDTO) {
    const component = await this.edgeComponentsService.assignComponent(dto);
    return component;
  }

  /**
   *
   * @api {post} /edge-components/remove-dependent Unassign dependent
   * @apiName UnassignDependent
   * @apiGroup EdgeComponents
   * @apiVersion 0.0.2
   * @apiDescription Detaches a dependent component from an edge component
   * @apiParam {string} uuid The uuid of the edge component
   * @apiParam {string} dependent The uuid of the dependent component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('remove-dependent')
  async removeDependentComponent(@Body() dto: UnassignComponentDTO) {
    const component = this.edgeComponentsService.unassignComponent(dto);
    return component;
  }
}
