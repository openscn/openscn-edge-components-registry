import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EdgeComponentSchema } from './edge-component.schema';
import { EdgeComponentsController } from './edge-components-controller/edge-components.controller';
import { EdgeComponentsService } from './edge-components-service/edge-components.service';
import { DependentComponentsModule } from './../dependent-components/dependent-components.module';

@Module({
  imports: [
    DependentComponentsModule,
    MongooseModule.forFeature([
      { name: 'EdgeComponent', schema: EdgeComponentSchema },
    ]),
  ],
  controllers: [EdgeComponentsController],
  providers: [EdgeComponentsService],
})
export class EdgeComponentsModule {}
