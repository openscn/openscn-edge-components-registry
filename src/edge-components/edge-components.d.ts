/**
 *
 * An object to represent health or trust statuses
 *
 */
export interface StatusUnit {
  status: string;
  updatedAt: Date;
}
