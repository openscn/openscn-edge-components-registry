/**
 *
 * Dependent component schema
 *
 * A dependtent component is a component that is attached to an edge component.
 * That could be a sensor, an actuator or an actino initiator
 *
 */

import * as mongoose from 'mongoose';

export const DependentComponentsSchema: mongoose.Schema = new mongoose.Schema(
  {
    uuid: String,
    valueType: String,
    action: String,
    type: String,
    isActive: Boolean,
    createdAt: Date,
    updatedAt: Date,
  },
  {
    collection: 'dependent-components',
  },
);
