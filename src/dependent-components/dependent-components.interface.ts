import { Document } from 'mongoose';

/**
 *
 * A dependent component
 * @interface
 * @property {string} uuid The unique identifier of the component
 * @property {string} valueType The value type the the component posts
 * @property {string} name The name of the component
 * @property {sting} action The action the the post initiates
 * @property {string} type The type of the dependent component
 * @property {boolean} isActive A flag the indicates if the component is active
 * @property {Date} createdAt The date the component was created
 * @property {Date} updatedAt The date the component was  updated
 *
 */
export interface DependentComponent extends Document {
  uuid: string;
  valueType: string;
  action: string;
  type: string;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
}
