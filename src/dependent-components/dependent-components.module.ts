import { Module } from '@nestjs/common';
import { DependentComponentsSchema } from './dependent-components.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DependentComponentsController } from './dependent-components-controller/dependent-components.controller';
import { DependentComponentsService } from './dependent-components-service/dependent-components.service';
import { AuthorizationModule } from './../lib/authorization/authorization.module';

@Module({
  imports: [
    AuthorizationModule,
    MongooseModule.forFeature([
      { name: 'DependentComponent', schema: DependentComponentsSchema },
    ]),
  ],
  providers: [DependentComponentsService],
  controllers: [DependentComponentsController],
  exports: [DependentComponentsService],
})
export class DependentComponentsModule {}
