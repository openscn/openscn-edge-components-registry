import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { DependentComponentsService } from './dependent-components.service';
import {
  StorageTestingModule,
  closeDatabase,
} from './../../lib/storage/storage-testing.module';
import { DependentComponentsSchema } from './../dependent-components.schema';

describe('DependentComponentsService', () => {
  let service: DependentComponentsService;
  let module: TestingModule;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        StorageTestingModule,
        MongooseModule.forFeature([
          {
            name: 'DependentComponent',
            schema: DependentComponentsSchema,
          },
        ]),
      ],
      providers: [DependentComponentsService],
    }).compile();

    service = module.get<DependentComponentsService>(
      DependentComponentsService,
    );
  });

  afterAll(async () => {
    await closeDatabase();
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('The getActiveStatus function', () => {
    it('should return true  if the isActive is true', () => {
      const dto = {
        isActive: true,
        valueType: 'mock',
        action: 'mock',
        type: 'sensor',
      };

      const result = service.getIsActiveStatus(dto);
      expect(result).toBe(true);
    });

    it('should return false if the isActive is false', () => {
      const dto = {
        isActive: false,
        valueType: 'mock',
        action: 'mock',
        type: 'sensor',
      };

      const result = service.getIsActiveStatus(dto);
      expect(result).toBe(false);
    });

    it('should return true (default value) if the isActive is not defined', () => {
      const dto = {
        isActive: undefined,
        valueType: 'mock',
        action: 'mock',
        type: 'sensor',
      };

      const dto2 = {
        isActive: null,
        valueType: 'mock',
        action: 'mock',
        type: 'sensor',
      };

      const result = service.getIsActiveStatus(dto);
      const result2 = service.getIsActiveStatus(dto2);
      expect(result).toBe(true);
      expect(result2).toBe(true);
    });
  });
});
