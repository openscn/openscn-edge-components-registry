import {
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { v4 as uuidv4 } from 'uuid';
import { Model } from 'mongoose';
import { DependentComponent } from './../dependent-components.interface';
import {
  CreateDependentComponentDTO,
  UpdateDependentComponentDTO,
} from './../dto/dependent-components.dto';

@Injectable()
export class DependentComponentsService {
  constructor(
    @InjectModel('DependentComponent')
    private dependentComponentModel: Model<DependentComponent>,
  ) {}

  async findOne(uuid: string) {
    let component: DependentComponent;
    try {
      component = await this.dependentComponentModel.findOne({ uuid });
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error during dependent component fetching ${uuid}`,
      );
    }

    if (!component) {
      throw new NotFoundException(
        'NOTFOUNDERROR',
        `Dependent component ${uuid} not found`,
      );
    }

    return component;
  }

  async findMany(uuids: string[]) {
    let components: DependentComponent[];

    try {
      components = await this.dependentComponentModel.find({
        uuid: { $in: uuids },
      });
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error during dependent component reading`,
      );
    }

    if (components) {
      return components;
    } else {
      throw new NotFoundException(
        `NOTFOUNDERROR`,
        'Dependent components could not be found',
      );
    }
  }

  async create(dto: CreateDependentComponentDTO) {
    const component = new this.dependentComponentModel({
      uuid: uuidv4(),
      isActive: this.getIsActiveStatus(dto),
      action: dto.action,
      type: dto.type,
      valueType: dto.valueType,
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    try {
      await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error  while saving dependent component`,
      );
    }

    return component;
  }

  async updateOne(dto: UpdateDependentComponentDTO) {
    let component: DependentComponent;
    const uuid = dto.uuid;

    try {
      component = await this.dependentComponentModel.findOne({
        uuid: dto.uuid,
      });
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error while fetching dependent component ${dto.uuid}`,
      );
    }

    delete dto.uuid;

    for (const attribute of Object.keys(dto)) {
      if (dto[attribute]) {
        component[attribute] = dto[attribute];
      }
    }
    component.isActive = this.getIsActiveStatus(dto);
    component.updatedAt = new Date();

    try {
      await component.save();
    } catch (err) {
      throw new InternalServerErrorException(
        'DBERROR',
        `Error while updating component ${uuid}`,
      );
    }

    return component;
  }

  async deleteOne(uuid: string) {
    let component: DependentComponent;
    try {
      component = await this.dependentComponentModel.findOne({ uuid });
    } catch (err) {
      throw new NotFoundException(
        `NOTFOUNDERROR`,
        `Dependent component ${uuid} was not found`,
      );
    }

    try {
      await component.remove();
    } catch (err) {
      throw new InternalServerErrorException(
        `DBERROR`,
        `Dependent component ${uuid} could not be deleted`,
      );
    }

    return component;
  }

  /**
   *
   * Get the isActive status of the  request
   *
   * @param createDependentComponentDTO
   *
   */
  getIsActiveStatus(
    dto: CreateDependentComponentDTO | UpdateDependentComponentDTO,
  ): boolean {
    let isActive: boolean;

    if (
      Object.keys(dto).indexOf('isActive') >= 0 &&
      typeof dto.isActive === 'boolean'
    ) {
      isActive = dto.isActive;
    } else {
      isActive = true;
    }

    return isActive;
  }
}
