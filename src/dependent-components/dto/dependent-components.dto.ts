import {
  IsString,
  IsBoolean,
  IsIn,
  IsUUID,
  IsArray,
  IsOptional,
} from 'class-validator';

export class CreateDependentComponentDTO {
  @IsBoolean()
  isActive: boolean;

  @IsString()
  @IsIn(['int', 'dec', 'string'])
  valueType: string;

  @IsString()
  action: string;

  @IsString()
  type: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateDependentComponentDTO {
  @IsOptional()
  @IsUUID(4)
  uuid: string;

  @IsOptional()
  @IsBoolean()
  isActive: boolean;

  @IsOptional()
  @IsString()
  @IsIn(['int', 'dec', 'string'])
  valueType: string;
}

// tslint:disable-next-line: max-classes-per-file
export class DeleteDependentComponentDTO {
  @IsUUID(4)
  uuid: string;
}

// tslint:disable-next-line: max-classes-per-file
export class GetOneDependentComponentDTO {
  @IsUUID(4)
  uuid: string;
}

// tslint:disable-next-line: max-classes-per-file
export class GetMenyDependentsComponentDTO {
  @IsArray()
  uuids: string[];
}
