import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { DependentComponentsController } from './dependent-components.controller';
import { DependentComponentsService } from './../dependent-components-service/dependent-components.service';
import { DependentComponentsSchema } from '../dependent-components.schema';
import {
  StorageTestingModule,
  closeDatabase,
} from './../../lib/storage/storage-testing.module';

describe('DependentComponents Controller', () => {
  let controller: DependentComponentsController;
  let module: TestingModule;

  beforeAll(() => {
    process.env.OPENSCN_EC_MNG_TOKEN = 'mockToken';
    process.env.OPENSCN_EC_EC_TOKEN = 'mockToken';
  });

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        StorageTestingModule,
        MongooseModule.forFeature([
          {
            name: 'DependentComponent',
            schema: DependentComponentsSchema,
          },
        ]),
      ],
      providers: [DependentComponentsService],
      controllers: [DependentComponentsController],
    }).compile();

    controller = module.get<DependentComponentsController>(
      DependentComponentsController,
    );
  });

  afterAll(async () => {
    await closeDatabase();
    await module.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
