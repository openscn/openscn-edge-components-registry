import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import {
  CreateDependentComponentDTO,
  UpdateDependentComponentDTO,
  DeleteDependentComponentDTO,
  GetOneDependentComponentDTO,
  GetMenyDependentsComponentDTO,
} from '../dto/dependent-components.dto';
import { DependentComponentsService } from './../dependent-components-service/dependent-components.service';
import { ManagementApiTokenGuard } from './../../lib/authorization';

@Controller('dependent-components')
@UseGuards(ManagementApiTokenGuard)
export class DependentComponentsController {
  constructor(
    private readonly dependentComponentsService: DependentComponentsService,
  ) {}

  /**
   *
   * @api {post} /dependent-components/get-one Get one
   * @apiName Show
   * @apiGroup DependentComponents
   * @apiVersion 0.0.2
   * @apiDescription Get one component
   * @apiParam {string} uuid The name of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('get-one')
  async getOne(@Body() dto: GetOneDependentComponentDTO) {
    try {
      const component = await this.dependentComponentsService.findOne(dto.uuid);
      return component;
    } catch (err) {
      throw err;
    }
  }

  /**
   *
   * @api {post} /dependent-components/get-many Get many
   * @apiName List
   * @apiGroup DependentComponents
   * @apiVersion 0.0.2
   * @apiDescription Get many components
   * @apiParam {string[]} uuids The name of the edge component
   *
   * @apiUse apiTokenHeader
   * @apiuse notFoundError
   * @apiUse internalError
   *
   */
  @Post('get-many')
  async getMany(@Body() dto: GetMenyDependentsComponentDTO) {
    try {
      const components = await this.dependentComponentsService.findMany(
        dto.uuids,
      );
      return components;
    } catch (err) {
      throw err;
    }
  }

  /**
   *
   * @api {post} /dependent-components/create Create component
   * @apiName Create
   * @apiGroup DependentComponents
   * @apiVersion 0.0.2
   * @apiDescription Create a single component
   * @apiParam {boolean} [isActive=true] The status of the component
   * @apiParam {string} [valueType] The value type of the component
   *
   * @apiUse apiTokenHeader
   * @apiUse internalError
   *
   */
  @Post('create')
  async create(@Body() dto: CreateDependentComponentDTO) {
    const component = await this.dependentComponentsService.create(dto);
    return component;
  }

  /**
   *
   * @api {post} /dependent-components/update Update component
   * @apiName Update
   * @apiGroup DependentComponents
   * @apiVersion 0.0.2
   * @apiDescription Update a single component
   * @apiParam {string} uuid The uuid of the component
   * @apiParam {boolean} [isActive] The status of the component
   * @apiParam {string} [valueType] The valueType of the component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse conflictError
   * @apiUse internalError
   *
   */
  @Post('update')
  async update(@Body() dto: UpdateDependentComponentDTO) {
    const component = await this.dependentComponentsService.updateOne(dto);
    return component;
  }

  /**
   *
   * @api {post} /dependent-components/delete Delete component
   * @apiName Delete
   * @apiGroup DependentComponents
   * @apiVersion 0.0.2
   * @apiDescription Delete a single component
   * @apiParam {string} uuid The uuid of the component
   *
   * @apiUse apiTokenHeader
   * @apiUse notFoundError
   * @apiUse internalError
   *
   */
  @Post('delete')
  async delete(@Body() dto: DeleteDependentComponentDTO) {
    const component = await this.dependentComponentsService.deleteOne(dto.uuid);
    return component;
  }
}
