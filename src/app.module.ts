import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StorageModule } from './lib/storage/storage.module';
import { EdgeComponentsModule } from './edge-components/edge-components.module';
import { AuthorizationStrategyModule } from './authorization-strategy/authorization-strategy.module';
import { DependentComponentsModule } from './dependent-components/dependent-components.module';

@Module({
  imports: [
    StorageModule,
    EdgeComponentsModule,
    AuthorizationStrategyModule,
    DependentComponentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
