import { EdgeComponentsApiTokenGuard } from './edge-components-api-token.guard';
import { EDGE_COMPONENTS_TOKEN_ENV_VAR_NAME } from './../../authorization.constants';

describe('EdgeComponentsApiTokenGuard', () => {
  it('should be defined', () => {
    process.env[EDGE_COMPONENTS_TOKEN_ENV_VAR_NAME] = 'mock';
    expect(new EdgeComponentsApiTokenGuard()).toBeDefined();
  });
});
