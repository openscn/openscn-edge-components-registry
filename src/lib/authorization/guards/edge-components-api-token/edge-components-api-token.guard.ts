import { CanActivate, Injectable } from '@nestjs/common';
import { ApiTokenGuard } from './../api-token/api-token.guard';
import {
  EDGE_COMPONENTS_TOKEN_ENV_VAR_NAME,
  EDGE_COMPONENTS_TOKEN_HEADER_NAME,
} from './../../authorization.constants';

@Injectable()
export class EdgeComponentsApiTokenGuard extends ApiTokenGuard
  implements CanActivate {
  constructor() {
    super(
      EDGE_COMPONENTS_TOKEN_ENV_VAR_NAME,
      EDGE_COMPONENTS_TOKEN_HEADER_NAME,
    );
  }
}
