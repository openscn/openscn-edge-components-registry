import { ManagementApiTokenGuard } from './management-api-token.guard';
import { MANAGEMENT_TOKEN_ENV_VAR_NAME } from './../../authorization.constants';

describe('ManagementApiTokenGuard', () => {
  it('should be defined', () => {
    process.env[MANAGEMENT_TOKEN_ENV_VAR_NAME] = 'mock';
    expect(new ManagementApiTokenGuard()).toBeDefined();
  });
});
