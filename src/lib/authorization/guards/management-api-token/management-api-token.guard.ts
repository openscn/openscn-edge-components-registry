import { CanActivate, Injectable } from '@nestjs/common';
import { ApiTokenGuard } from './../api-token/api-token.guard';
import {
  MANAGEMENT_TOKEN_ENV_VAR_NAME,
  MANAGEMENT_TOKEN_HEADER_NAME,
} from './../../authorization.constants';

@Injectable()
export class ManagementApiTokenGuard extends ApiTokenGuard
  implements CanActivate {
  constructor() {
    super(MANAGEMENT_TOKEN_ENV_VAR_NAME, MANAGEMENT_TOKEN_HEADER_NAME);
  }
}
