import {
  CanActivate,
  ExecutionContext,
  Injectable,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export abstract class ApiTokenGuard implements CanActivate {
  private apiTokenName: string;
  private apiTokenHeader: string;

  constructor(apiTokenName: string, apiTokenHeader: string) {
    if (!!!apiTokenName) {
      throw new InternalServerErrorException('Api token should be defined');
    } else if (!!!apiTokenHeader) {
      throw new InternalServerErrorException(
        'Api token header should be defined',
      );
    } else if (!!!process.env[apiTokenName]) {
      throw new InternalServerErrorException(
        `${apiTokenName} coould not be found in env`,
      );
    }

    this.apiTokenName = apiTokenName;
    this.apiTokenHeader = apiTokenHeader;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const headers = context.getArgByIndex(0).headers;
    const serverToken = process.env[this.apiTokenName];
    return this.compareTokens(serverToken, headers);
  }

  compareTokens(serverToken: string, headers): boolean {
    if (Object.keys(headers).indexOf(this.apiTokenHeader) >= 0) {
      return serverToken === headers[this.apiTokenHeader];
    } else {
      throw new BadRequestException(
        `Header ${this.apiTokenHeader} is not found`,
      );
    }
  }
}
