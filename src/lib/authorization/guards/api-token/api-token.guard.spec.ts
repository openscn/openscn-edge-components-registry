import { ApiTokenGuard } from './api-token.guard';

describe('ApiTokenGuard', () => {
  process.env.MOCK_TOKEN_ENV_VAR = 'mockToken';
  class MockGuardClass extends ApiTokenGuard {
    constructor() {
      super('MOCK_TOKEN_ENV_VAR', 'x-mock-header');
    }
  }

  let mockGuard: MockGuardClass;

  beforeEach(() => {
    mockGuard = new MockGuardClass();
  });

  it('should be defined', () => {
    expect(mockGuard).toBeDefined();
  });

  it('should throw an error if the token name is not defined', () => {
    // tslint:disable-next-line: max-classes-per-file
    const newMockClass = class NewMockClass extends ApiTokenGuard {
      constructor() {
        super(null, null);
      }
    };
    expect(() => new newMockClass()).toThrow();
  });

  it('should throw error if the api token is not at the headers', async () => {
    expect(() => mockGuard.compareTokens('mockToken', {})).toThrow();
  });

  it('should return true for matched tokens', () => {
    const mockHeaders = {
      'x-mock-header': 'mockmock',
    };

    const result = mockGuard.compareTokens('mockmock', mockHeaders);
    expect(result).toBe(true);
  });

  it('should throw bad format error if the token is missing', () => {
    expect(() => mockGuard.compareTokens('mockmock', {})).toThrow(
      `Header x-mock-header is not found`,
    );
  });
});
