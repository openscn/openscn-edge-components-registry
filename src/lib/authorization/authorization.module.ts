import { Module } from '@nestjs/common';
import { ManagementApiTokenGuard } from './guards/management-api-token/management-api-token.guard';
import { EdgeComponentsApiTokenGuard } from './guards/edge-components-api-token/edge-components-api-token.guard';

@Module({
  providers: [ManagementApiTokenGuard, EdgeComponentsApiTokenGuard],
  exports: [ManagementApiTokenGuard, EdgeComponentsApiTokenGuard],
})
export class AuthorizationModule {}
