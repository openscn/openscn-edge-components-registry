import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv';
dotenv.config();

const mongooseConnection = {
  host: process.env.OPENSCN_MONGO_HOST,
  user: process.env.OPENSCN_MONGO_USER,
  password: process.env.OPENSCN_MONGO_PASSWORD,
  database: process.env.OPENSCN_MONGO_DATABASE,
};

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: `mongodb://${mongooseConnection.user}:${mongooseConnection.password}@${mongooseConnection.host}/${mongooseConnection.database}`,
        connectionName: 'openscn-edge-components-registry',
      }),
    }),
  ],
  providers: [],
})
export class StorageModule {}
