export interface AuthorizationStrategyInterface {
  dataFlow: string;
  authorize: () => {};
  getTrustStatus: () => {};
}
