/**
 *
 * The payload is not the expected one
 *
 */
export class BadPayloadFormatError extends Error {
  constructor(message?: string) {
    super(message);
  }
}
