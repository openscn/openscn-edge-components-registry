import { Test, TestingModule } from '@nestjs/testing';
import { AuthorizationStrategyService } from './authorization-strategy.service';

describe('AuthorizationStrategyService', () => {
  let service: AuthorizationStrategyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthorizationStrategyService],
    }).compile();

    service = module.get<AuthorizationStrategyService>(
      AuthorizationStrategyService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
