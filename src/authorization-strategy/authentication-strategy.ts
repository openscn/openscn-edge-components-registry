/**
 *
 * Provides the base for all authentication strategies
 *
 */

abstract class AuthenticationStrategy {
  dataFlow: string;
  authorize() {}
  getTrustStatus() {}
}
